import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';

class HelloWorld extends React.Component {
  render() {
    return <div>Hello world!</div>;
  }
}

describe('(Component) HelloWorld', () => {
  it('renders as div', () => {
    const wrapper = shallow(<HelloWorld />);
    expect(wrapper.type()).to.eql('div');
  });

  it('contains text "Hello world!"', () => {
    const wrapper = shallow(<HelloWorld />);
    expect(wrapper.contains('Hello world!')).to.equal(true);
  });
});
