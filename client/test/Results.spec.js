import { expect } from 'chai';

const SEARCH_STARTED = 'SEARCH_STARTED';
const SEARCH_RESULTS_RECEIVED = 'SEARCH_RESULTS_RECEIVED';

function results(state = {}, action) {
  switch (action.type) {
    case SEARCH_STARTED:
      return {};

    case SEARCH_RESULTS_RECEIVED:
      return action.results;

    default:
      return state;
  }
}

describe('(Reducer) results', () => {
  it('retains state on unknown actions', () => {
    const initialState = {'eggs': 0};
    let nextState = results(initialState, { type: 'BANANA' });
    expect(nextState).to.eql(initialState);
  });

  it('clears results when search starts', () => {
    let nextState = results({'eggs': 0}, { type: SEARCH_STARTED });
    expect(nextState).to.eql({});
  });

  it('stores results when search results are received', () => {
    let searchResults = {
      'eggs': 0, 'ham': 1, 'bacon': 2
    };

    let nextState = results(
      {},
      {
        type: SEARCH_RESULTS_RECEIVED,
        results: searchResults
      }
    );
    expect(nextState).to.equal(searchResults);
  });
});
