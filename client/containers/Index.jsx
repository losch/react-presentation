var React = require('react');

var Default = React.createClass({
  render: function() {
    return (
      <html>
      <head>
        <meta charSet="utf-8"></meta>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"></meta>
        <title>Reactin perusteet</title>
      </head>
      <body style={{fontFamily: ['Varela Round', 'sans-serif']}}>
      <div id="app" />
      <script src="/static/bundle.js"></script>
      </body>
      </html>
    );
  }
});

module.exports = Default;
