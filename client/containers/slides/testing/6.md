React storybookin asennus
=========================

  ```
  npm i --save-dev @kadira/storybook
  ```
package.jsoniin skripti:
  ```
  "scripts": {
    "storybook": "start-storybook -p 9001"
  }
  ```
Luodaan konfiguraatio (.storybook/config.js):
  ```javascript
  import { configure } from '@kadira/storybook';

  function loadStories() {
    require('../client/stories/button');
  }

  configure(loadStories, module);
  ```

Ja stories-tiedostoja:
  ```javascript
  import React from 'react';
  import { storiesOf, action } from '@kadira/storybook';

  storiesOf('Button', module)
    .add('with text', () => (
      <button onClick={action('clicked')}>My First Button</button>
    ))
    .add('with no text', () => (
      <button></button>
    ));
  ```