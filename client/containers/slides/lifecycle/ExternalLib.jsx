import React from 'react';
import ReactDOM from 'react-dom';

export default class ExternalLib extends React.Component {
  shouldComponentUpdate() {
    return false;
  }

  componentDidMount() {
    this.onClick = e =>
      e.target.innerHTML = 'Button clicked!';

    let button = document.createElement('button');
    let text = document.createTextNode('Click me');
    button.appendChild(text);
    button.addEventListener('click', this.onClick);
    this.button = button;

    ReactDOM.findDOMNode(this.refs.buttonContainer)
            .appendChild(button);
  }

  componentWillUnmount() {
    this.button.removeEventListener('click', this.onClick);
  }

  render() {
    return <div ref="buttonContainer" />;
  }
}
