Elinkaarimetodit - mixinit
==========================

* React-komponenteille voidaan luoda ns. mixineitä, jotka kytkeytyvät
  lifecycle-metodeihin
* ES6-React-komponenttien kanssa mixineitä ei voi käyttää ollenkaan
* Yleensä mixineitä ei tarvitse, vaan vastaavan toiminnallisuuden pystyy
  tekemään selkeämmin erillisessä funktiossa, jolla voi wrapata komponentteja
