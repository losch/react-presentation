import React from 'react';

var SetIntervalMixin = {
  componentWillMount: function() {
    this.intervals = [];
  },
  setInterval: function() {
    this.intervals.push(setInterval.apply(null, arguments));
  },
  componentWillUnmount: function() {
    this.intervals.forEach(clearInterval);
  }
};

var TickTock = React.createClass({
  mixins: [SetIntervalMixin],
  getInitialState: function() {
    return {
      seconds: 0,
      seconds2: 0
    };
  },
  componentDidMount: function() {
    this.setInterval(this.tick, 1000);
    this.setInterval(this.tick2, 500);
  },
  tick: function() {
    this.setState({seconds: this.state.seconds + 1});
  },
  tick2: function() {
    this.setState({seconds2: this.state.seconds2 + 1});
  },
  render: function() {
    return (
      <div>
        <p>Timer #1: {this.state.seconds}</p>
        <p>Timer #2: {this.state.seconds2}</p>
      </div>
    );
  }
});

export default TickTock;