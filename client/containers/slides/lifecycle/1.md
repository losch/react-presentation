Elinkaarimetodit (lifecycle methods)
====================================

* React-komponenteilla on metodeita joita kutsutaan tietyissä hetkissä kun
  komponentille tapahtuu asioita
* Elinkaarimetodeissa voidaan esim. muokata DOM:ia (tietyin rajoittein) Reactin
  ulkopuolella
