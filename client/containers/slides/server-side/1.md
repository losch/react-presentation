Serveripuolen renderöinti
=========================

* Reactia voi myös renderöidä serverillä
* Yleinen käyttötarkoitus on nopeuttaa yhden sivun sovelluksen ensimmäistä
  sivulatausta
  1. Palvelin renderöi React-koodin ja palauttaa sivun HTML-muodossa
  2. Sivun lopussa ladataan JavaScript-bundle
  3. Kun bundle käynnistyy, React-komponentit mountataan sivulle
  4. React tekee diffauksen mitä täytyy sivulta muuttaa ja ottaa sivun haltuun
* Koodi voi tässä tapauksessa olla serveri- ja client-puolella täsmälleen sama
* Renderöintiin tarvitaan NodeJS, Nashorn tai muu JavaScript-moottori
