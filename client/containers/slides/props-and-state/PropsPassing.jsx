import React from 'react';
const PropTypes = React.PropTypes;

export default class CardDemo extends React.Component {
  render() {
    return (
      <div>
        <Card title="Hieno kortti"
              content="Hienolla sisällöllä"/>
        <Card title="Hienompi kortti"
              content="Hienommalla sisällöllä"/>
      </div>
    );
  }
}

class Card extends React.Component {
  render() {
    return (
      <div className="mui-panel">
        <Title text={this.props.title} />
        <Body text={this.props.content} />
      </div>
    );
  }
}

Card.propTypes = {
  title: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired
};

class Title extends React.Component {
  render() {
    return <h1>{this.props.text}</h1>;
  }
}

Title.propTypes = {
  text: PropTypes.string.isRequired
};

class Body extends React.Component {
  render() {
    return <p>{this.props.text}</p>;
  }
}

Body.propTypes = {
  text: PropTypes.string.isRequired
};

