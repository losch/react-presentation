import React from 'react';

export default class SecretCode extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      code: this.randomCode(),
      isCodeVisible: true
    };
  }

  randomCode() {
    return Math.random().toString(36).substring(2,9);
  }

  randomChar() {
    return Math.random().toString(36).substring(2,3);
  }

  replaceAt(str, index, character) {
    return str.substring(0, index) +
           character +
           str.substring(index + character.length);
  }

  randomize(str) {
    let index = Math.floor(Math.random() * str.length);
    return this.replaceAt(str, index, this.randomChar());
  }
  
  componentDidMount() {
    this.timer = setInterval(() => {
      if (this.state.isCodeVisible) {
        this.setState({
          isCodeVisible: false
        });
      }
      else {
        this.setState({
          isCodeVisible: true,
          code: this.randomize(this.state.code)
        });
      }
    }, 600);
  }
  
  componentWillUnmount() {
    clearInterval(this.timer);
  }

  render() {
    let codeStyle = !this.state.isCodeVisible ? { display: 'none' } : {};
    let code = <b style={codeStyle}>{this.state.code}</b>;
    return (
      <div>For unsubscribing from spam list,
           please enter the following code: <b>{code}</b></div>
    );
  }
}