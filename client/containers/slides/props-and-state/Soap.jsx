import React from 'react';

export default class Soap extends React.Component {
  render() {
    let imgStyle = {maxHeight: '400px', marginRight: '50px'};
    return (
      <div>
        <h1>{this.props.title}</h1>
        <div style={{textAlign: 'center'}}>
          <img style={imgStyle} src="/static/sock.jpg" />
          <span style={{fontSize: '128px'}}>+</span>
          <img style={imgStyle} src="/static/soap.jpg" />
          <p>Seuraava kehittäjä saattaa rakentaa näistä osista kätevän
             työkalun</p>
        </div>
      </div>
    );
  }
}
