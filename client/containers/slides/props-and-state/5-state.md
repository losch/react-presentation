Komponentin sisäinen tila
=========================

Komponentit voivat pitää sisäisen tilan. Hyviä käyttötarkoituksia tälle on esim.
dropdown menun tila onko se auki vai kiinni.

Komponentin tilankäsittelyn säännöt:

1. Tila initialisoidaan kun komponentti luodaan
2. Tilaa muutetaan kutsumalla komponentin `setState`-metodia
3. `setState`:n kutsuminen aiheuttaa komponentin uudelleenrenderöinnin.

  ```javascript
  setState(newState, callback)
  ```

4. `setState`:lle voi antaa callback-funktion jota kutsutaan kun tila on
   muutettu ja komponentti on uudelleenrenderöity
5. `setState`-metodi mergeää nykyisen tilan ja newStaten sisältämät arvot

  ```javascript
  state = { eggs: 1, ham: 2, bacon: 3 };

  setState({ ham: 100 });

  =>

  state: { eggs: 1, ham: 100, bacon: 3 }
  ```
