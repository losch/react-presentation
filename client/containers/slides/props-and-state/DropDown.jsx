import React from 'react';
import Classnames from 'classnames';

export class DropDownButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isOpen: false }
  }

  toggle() {
    this.setState({ isOpen: !this.state.isOpen });
  }

  render() {
    let dropDownClassName =
      Classnames('mui-dropdown__menu',
                 { 'mui--is-open': this.state.isOpen });
    return (
      <div className="mui-dropdown">
        <button className="mui-btn mui-btn--primary"
                onClick={() => this.toggle()}>
          { this.props.text }
          <span className="mui-caret" />
        </button>
        <ul className={dropDownClassName}>
          {
            React.Children.map(this.props.children,
                               item => <li>{item}</li>)
          }
        </ul>
      </div>
    );
  }
}

export default class BreakfastMenu extends React.Component {
  render() {
    return (
      <DropDownButton text="Breakfast menu">
        <a>Eggs</a>
        <a>Ham</a>
        <a>Bacon</a>
        <a>More bacon</a>
      </DropDownButton>
    );
  }
}
