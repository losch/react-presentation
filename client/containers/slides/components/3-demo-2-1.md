Esimerkki: React-komponenttiin sisältöä
=======================================

Komponentti:
```javascript
class Hello extends React.Component {
  render() {
    return <div>Hello {this.props.name}!</div>
  }
}

// Vaihtoehtoisesti sama funktiona
const Hello2 = ({name}) => <div>Hello {name}!</div>;
```

Renderöidään komponentti selaimen DOM:n:
```javascript
ReactDOM.render(<Hello name="World" />, document.getElementById('app'));
```

Lopputulos:
```
Hello World!
```
