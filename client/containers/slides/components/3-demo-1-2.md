Esimerkki: Yksinkertainen React-komponentti
===========================================

index.html
```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <title>Yksinkertainen React-komponentti</title>
  </head>
  <body>
    <div id="app" />
    <script src="/bundle.js"></script>
  </body>
</html>
```

index.js
```javascript
import React from 'react';
import ReactDOM from 'react-dom';

class HelloWorld extends React.Component {
  render() {
    return <div>Hello world!</div>;
  }
}

ReactDOM.render(<HelloWorld />, document.getElementById('app'));
```
