Esimerkki: Yksinkertainen React-komponentti
===========================================

```javascript
// ES5
var HelloWorldEs5 = React.createClass({
  render: function() {
    return React.createElement('div', null, 'Hello world!');
  }
});

// ES5 + JSX
var HelloWorldEs5Jsx = React.createClass({
  render: function() {
    return <div>Hello world!</div>;
  }
});

// ES6 + JSX
class HelloWorldEs6 extends React.Component {
  render() {
    return <div>Hello world!</div>;
  }
}

// ES6 + JSX, "stateless functional component"
const HelloWorldEs6Func = () => <div>Hello world!</div>;
```
