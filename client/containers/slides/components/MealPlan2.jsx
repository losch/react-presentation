import React from 'react';

class Menu extends React.Component {
  render() {
    return (
      <div>
        <h1>{this.props.title}</h1>
        <ul>
          {
            React.Children.map(
              this.props.children,
              (item, i) => <li key={'item-' + i}>{item}</li>
            )
          }
        </ul>
      </div>
    );
  }
}

export default class MealPlan extends React.Component {
  render() {
    return (
      <div>
        <Menu title="Breakfast">
          <b>Eggs</b>
          <i>Ham</i>
          <u>Bacon</u>
        </Menu>
        <Menu title="Lunch">Pizza</Menu>
        <Menu title="Supper">
          <span>Sausages</span>
          <span style={{backgroundColor: 'lightgreen'}}>
            Cabbage
          </span>
        </Menu>
      </div>
    );
  }
}
