Komponentin key- ja ref-attribuutit
===================================

* key-attribuutti auttaa Reactia diffausvaiheessa päättelemään miten selaimen
  DOM:ia kuuluu muuttaa
  - Asetetaan jos renderöidään komponentteja listana
  - Täytyy olla komponentin sisäisesti uniikki
  - Reactin kehitysversio varoittaa jos key-attribuutin unohtaa

* ref-attribuutin avulla pääsee renderöityyn React-komponenttiin käsiksi
  - ReactDOM.findDOMNode-funktion avulla löytää oikean noden HTML:stä
