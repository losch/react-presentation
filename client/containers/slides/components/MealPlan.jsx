import React from 'react';

class Menu extends React.Component {
  render() {
    return (
      <div>
        <h1>{this.props.title}</h1>
        { 
          this.props.foods.map((item, i) =>
            <p key={"item-" + i}>{item}</p>
          ) 
        }
      </div>
    );
  }
}

export default class MealPlan extends React.Component {
  render() {
    return (
      <div>
        <Menu title="Breakfast" foods={['Eggs', 'ham', 'bacon']} />
        <Menu title="Lunch" foods={['Pizza']} />
        <Menu title="Supper" foods={['Broccoli', 'Bacon']} />
      </div>
    );
  }
}
