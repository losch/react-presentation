import React from 'react';

var HelloWorldEs5 = React.createClass({
  render: function() {
    return React.createElement('div', null, 'Hello world!')
  }
});

var HelloWorldEs5Jsx = React.createClass({
  render: function() {
    return <div>Hello world!</div>;
  }
});

class HelloWorldEs6 extends React.Component {
  render() {
    return <div>Hello world!</div>;
  }
}

const HelloWorldEs6Func = () => <div>Hello world!</div>;

export {
  HelloWorldEs5,
  HelloWorldEs5Jsx,
  HelloWorldEs6,
  HelloWorldEs6Func
};
