Esimerkki: children-property
============================

```javascript
class Menu extends React.Component {
  render() {
    return (
      <ul>
        <h1>{this.props.title}</h1>
        { React.Children.map(this.props.children, item => <li>{item}</li>) }
      </ul>
    );
  }
}

export default class MealPlan extends React.Component {
  render() {
    return (
      <div>
        <Menu title="Breakfast">
          <b>Eggs</b>
          <i>Ham</i>
          <u>Bacon</u>
        </Menu>
        <Menu title="Lunch">Pizza</Menu>
        <Menu title="Supper">
          <span>Broccoli</span>
          <span>More bacon</span>
          <span style={{display: 'inline-block',
                        transform: 'rotate(90deg)'}}>Behind the wall</span>
        </Menu>
      </div>
    );
  }
}
```
