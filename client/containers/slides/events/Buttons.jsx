import React from 'react';
import ReactDom from 'react-dom';

export default class Buttons extends React.Component {
  onClick(text) {
     ReactDom.findDOMNode(this.refs.display).innerHTML = text;
  }
  render() {
    return (
      <div>
        <button onClick={e => this.onClick('Button #1')}
                className="mui-btn">Button #1</button>
        <button onClick={e => this.onClick('Button #2')} 
                className="mui-btn mui-btn--primary">
          Button #2
        </button>
        <button onClick={e => this.onClick('Button #3')}
                className="mui-btn mui-btn--danger">
          Button #3
        </button>
        <span onClick={e => this.onClick('span')}>
          span
        </span>
        <div ref="display" />
      </div>
    );
  }
}
