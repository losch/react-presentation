import React from 'react';
import './Results.scss';

export default class Results extends React.Component {
  render() {
    if (this.props.isSearching) {
      return (
        <div className="results-is-searching">
          Haetaan elokuvia...
          <div className="inline-loader">
            <div className="loader"/>
          </div>
        </div>);
    }
    else if (this.props.results.Search &&
             this.props.results.Search.length > 0) {
      let results = this.props.results.Search;
      return (
        <table className="mui-table mui-table--bordered">
          <tbody>
          {
            results.map((result, i) =>
              <tr key={"result-" + i}>
                <td>{
                  result.Poster !== 'N/A' ?
                    <img className="poster"
                         src={result.Poster} /> : undefined
                }</td>
                <td>
                  <a href={'http://www.imdb.com/title/' +
                           result.imdbID}
                     target="_blank">
                    {result.Title}
                  </a>
                </td>
                <td>{result.Year}</td>
              </tr>
            )
          }
          </tbody>
        </table>
      );
    }
    else {
      return <div>Ei hakutuloksia.</div>;
    }
  }
}

const results = {
  "Search": [
    {
      "Title": "Mad Max: Fury Road",
      "Year": "2015",
      "imdbID": "tt1392190",
      "Poster": "N/A"
    },
    {
      "Title": "Mad Max",
      "Year": "1979",
      "imdbID": "tt0079501",
      "Poster": "N/A"
    }
  ]
};

export const ResultsDemo =
  () => <Results results={results} />;
