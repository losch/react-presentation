React-komponentin suunnittelu
=============================

![](/static/MovieSearch.png)

Input
-----

* Näyttää saadun arvon *value* propertystä
* Kun arvoa muutetaan, kutsuu *onChange* callback-funktiota uusi arvo funktion
  argumenttina

Results
-------

* Esittää hakutulokset *results* propertystä
* Jos *isSearching* property on *true*, niin näyttää tekstin
  "Etsitään elokuvia..."

MovieSearch
-----------

* Pitää tallessa tilaa mikä on nykyinen hakusana, onko haku menossa ja mitkä on
  rajapinnasta saadut viimeisimmät hakutulokset. MovieSearch-komponentti
  koostuu Input- ja Results-komponenteista
* Hakusanan muuttuessa lähettää rajapintaan hakukyselyn ja vastausten saapuessa
  antaa hakutulokset Results-komponentille renderöitäväksi