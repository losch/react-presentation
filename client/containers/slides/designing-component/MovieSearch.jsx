import React from 'react';
import request from 'superagent';
import debounce from 'lodash/debounce';
import './MovieSearch.scss';
import Input from './Input';
import Results from './Results';

const SEARCH_DELAY = 500; // ms

export function createSearch(cb) {
  return debounce(query =>
    request
      .get('/moviesearch/')
      .accept('application/json')
      .query({ s: query, type: 'movie' })
      .type('application/json')
      .end((err, res) => {
        cb(JSON.parse(res.text));
      }),
    SEARCH_DELAY);
}

export default class MovieSearch extends React.Component {
  constructor(props) {
    super(props);
    this.search = createSearch(res => this.onResults(res));
    this.state = {
      query: '',
      results: {},
      isSearching: false
    };
  }

  onResults(results) {
    this.setState({
      results: results,
      isSearching: false
    });
  }

  onQueryChange(query) {
    if (query === '') {
      this.setState({
        query: query,
        isSearching: false,
        results: {}
      });
    }
    else {
      this.search(query);
      this.setState({
        query: query,
        isSearching: true
      });
    }
  }

  render() {
    return (
      <div>
        <Input value={this.state.query}
               onChange={query => this.onQueryChange(query)} />
        <Results results={this.state.results}
                 isSearching={this.state.isSearching} />
      </div>
    );
  }
};
