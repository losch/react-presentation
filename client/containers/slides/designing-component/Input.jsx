import React from 'react';

export default class Input extends React.Component {
  clear() {
    this.props.onChange('');
    this.refs.input.focus();
  }

  render() {
    return (
      <div className="mui-textfield">
        <input type="text"
               ref="input"
               value={this.props.value}
               onChange={e => this.props.onChange(e.target.value)}
               placeholder="Etsi leffa" />
        <button className="mui-btn mui-btn--primary"
                onClick={e => this.clear()}>
          Tyhjennä
        </button>
      </div>
    );
  }
}

export const InputDemo =
  () => <Input value='' onChange={e => {}} />;
