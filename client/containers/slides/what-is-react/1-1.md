Miksi Reactia pitäisi käyttää?
==============================

* Yksinkertaistaa selaimen DOM:n käsittelyn. Kehittäjän täytyy ainoastaan
  kertoa miltä sovelluksen kuuluu missäkin tilanteessa näyttää ja React tekee
  tarvittavat muutokset DOM:n, sekä lisää tarvittavat tapahtumakäsittelijät.
* React piilottaa hyvin selainten väliset erot
* Monimutkaisten käyttöliittymätoimintojen tekeminen yksinkertaistuu
* Kaikki React-komponentit toimivat samalla periaatteella ja niiden rajapinta on
  pieni. Näistä syistä on helppo päätellä miten jokin komponentti toimii ja
  selvittää syy jos se ei toimi oikein.
* React-komponentteja on helppo käyttää ja vaikka kopioida projektista toiseen.
  Tästä syystä on olemassa React-komponenttikirjastoja, esim.:
  * React-bootstrap
  * Material UI
  * Elemental UI
* Suuri käyttäjäkunta: dokumentaatiota, komponenttiesimerkkejä ja kirjastoja
  löytyy paljon
* Hot loaderit. Ei enää sivun refreshausta, jotta koodimuutosten vaikutukset
  näkee, eli muutosten kokeileminen onnistuu nopeasti.
