Virtual-DOM
===========

* Selaimen DOM:n käsittely on hidasta. Sitä ei kannata luoda joka kerta tyhjästä
  uudelleen.
* Reactin renderöinti perustuu virtual-DOM:n käyttämiseen ja siihen, että
  selaimen DOM:ista muutetaan ainoastaan mitä tarvitsee muuttaa. Tästä syystä
  renderöinti tapahtuu nopeasti.
* React renderöi käyttöliittymän virtual-DOM:in ja tämän jälkeen suorittaa
  vertailun mitä selaimen DOM:sta täytyy muuttaa, jotta se vastaa virtual-DOM:n
  sisältöä.

Muita virtual-DOM-ideaa hyödyntäviä kirjastoja ja frameworkkeja
---------------------------------------------------------------

* [virtual-dom](https://github.com/Matt-Esch/virtual-dom) - Virtual-DOM-kirjasto
* [Riot](http://riotjs.com/) - Reactin kaltainen kirjasto, mutta pienempi
* [Mithril](http://mithril.js.org/) - MVC-framework, joka renderöi virtual DOM:n
* [Elm](http://elm-lang.org/) - Funktionaalinen kieli, joka kääntyy
  JavaScriptiksi. HTML-kirjasto toimii virtual-DOM-periaatteilla.
