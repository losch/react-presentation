Flux-arkkitehtuuri
==================

* Suosittu tapa Reactia käyttävissä sovelluksissa on käyttää Flux-arkkitehtuuria

![](/static/flux.png)

* **Action**: Sovelluksen tapahtumia. Esim. data vastaanotettu palvelimelta,
  käyttäjä käynnistänyt haun.
* **Dispatcher**: Ohjaa actionit storeille
* **Store**: Pitävät sovelluksen tilan tallessa ja muuttavat tilaa actioneiden
  perusteella
* **View**: Kuuntelevat storeja ja esittävät sovelluksen tilan
