import React from 'react';
import { createStore, combineReducers } from 'redux'
import { Provider, connect } from 'react-redux'
import debounce from 'lodash/debounce';
import { DevTools, enhancer } from './DevTools';
import { createSearch } from '../designing-component/MovieSearch';
import Input from '../designing-component/Input';
import Results from '../designing-component/Results';

//
// Action types
//

const SEARCH_STARTED = 'SEARCH_STARTED';
const SEARCH_RESULTS_RECEIVED = 'SEARCH_RESULTS_RECEIVED';


//
// Reducers & store
//

// Reducer for search parameters
function search(state = { query: '', isSearching: false }, action) {
  switch (action.type) {
    case SEARCH_STARTED:
      return {
        query: action.query,
        isSearching: action.isSearching
      };

    case SEARCH_RESULTS_RECEIVED:
      return {
        query: state.query,
        isSearching: false
      };

    default:
      return state;
  }
}

// Reducer for search results
function results(state = {}, action) {
  switch (action.type) {
    case SEARCH_STARTED:
      return {};

    case SEARCH_RESULTS_RECEIVED:
      return action.results;

    default:
      return state;
  }
}

// Combine reducers as a store
const rootReducer = combineReducers({search, results});

const store = createStore(
  rootReducer,
  {},
  enhancer
);


//
// Action creators
//

function doSearch(query, dispatch) {
  dispatch({
    type: SEARCH_STARTED,
    query: query,
    isSearching: query !== ''
  });

  if (query !== '') {
    createSearch(results =>
      dispatch({
        type: SEARCH_RESULTS_RECEIVED,
        results: results
      })
    )(query);
  }
}


//
// Containers
//

class MovieSearch_ extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      query: this.props.search.query || ''
    };

    this.delayedSearch = debounce(
      query => doSearch(query, this.props.dispatch),
      250
    );
    this.search = query => query === '' ?
                           doSearch(query, this.props.dispatch) :
                           this.delayedSearch(query);
  }

  onQueryChange(query) {
    this.setState({
      query: query
    });
    this.search(query);
  }

  render() {
    return (
      <div>
        <Input value={this.state.query}
               onChange={query => this.onQueryChange(query)}/>
        <Results results={this.props.results}
                 isSearching={this.props.search.isSearching}/>
      </div>
    );
  }
}

// Connect MovieSearch component to Redux
const mapStateToProps = (state) => {
  return {
    search: state.search,
    results: state.results
  };
};

const MovieSearch = connect(mapStateToProps)(MovieSearch_);

// Root container
const MovieSearchRedux = () =>
  <Provider store={store}>
    <div>
      <MovieSearch />
      <DevTools />
    </div>
  </Provider>;

export default MovieSearchRedux;
