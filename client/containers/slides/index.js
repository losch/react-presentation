/* eslint-disable react/display-name */

import React from 'react';
import MarkdownComponent from '../../components/MarkdownComponent';
import { HelloWorldEs6Func } from './components/HelloWorld';
import Soap from './props-and-state/Soap';
import End from './the-end/End';

if (module.hot) {
  module.hot.accept();
}

class Title extends React.Component {
  render() {
    return <div className="title-page"><h1>{this.props.children}</h1></div>;
  }
}

Title.propTypes = {
  children: React.PropTypes.array
};


class ComponentDemo extends React.Component {
  render() {
    let { src, element } = this.props;

    if (element) {
      return (
        <div>
          <h1>{this.props.title}</h1>
          <div className="mui-col-md-6 mui-panel">
            { <MarkdownComponent
              contents={'```javascript\n' + src + '\n```'}/> }
          </div>
          <div className="mui-col-md-6">
            { React.createElement(element, null) }
          </div>
        </div>
      );
    }
    else {
      return (
        <div>
          <h1>{this.props.title}</h1>
          <div className="mui-col-md-12 mui-panel">
            { <MarkdownComponent
              contents={'```javascript\n' + src + '\n```'}/> }
          </div>
        </div>
      );
    }
  }
}

ComponentDemo.propTypes = {
  title: React.PropTypes.string.isRequired,
  src: React.PropTypes.string.isRequired,
  element: React.PropTypes.element
};


export const SLIDES = [
  // What is React?

  () => <MarkdownComponent contents={require('raw-loader!./what-is-react/1.md')} />,
  () => <MarkdownComponent contents={require('raw-loader!./what-is-react/1-1.md')} />,
  () => <MarkdownComponent contents={require('raw-loader!./what-is-react/2-virtual-dom.md')} />,

  // Component demo

  () => <Title>Komponenttidemoja</Title>,
  () => <MarkdownComponent contents={require('raw-loader!./components/3-demo-1-1.md')} />,
  () => <MarkdownComponent contents={require('raw-loader!./components/3-demo-1-2.md')} />,
  () => <div className="mui-panel example-panel"><HelloWorldEs6Func /></div>,
  () => <MarkdownComponent contents={require('raw-loader!./components/3-demo-2-1.md')} />,
  () => <ComponentDemo
          title="Esimerkki: React-komponenttien yhdistäminen"
          src={require('!raw!./components/MealPlan.jsx')}
          element={require('./components/MealPlan.jsx').default} />,
  () => <ComponentDemo
          title="Esimerkki: React-komponenttien yhdistäminen #2"
          src={require('!raw!./components/MealPlan2.jsx')}
          element={require('./components/MealPlan2.jsx').default} />,
  () => <MarkdownComponent contents={require('raw-loader!./components/3.md')} />,
  () => <MarkdownComponent contents={require('raw-loader!./components/4.md')} />,
  () => <ComponentDemo
          title="Esimerkki: MarkdownComponent"
          src={require('!raw!../../components/MarkdownComponent.jsx')} />,

  // JSX

  () => <Title>JSX</Title>,
  () => <MarkdownComponent contents={require('raw-loader!./jsx/1.md')} />,
  () => <MarkdownComponent contents={require('raw-loader!./jsx/2.md')} />,

  // Event handling

  () => <Title>Tapahtumien käsittely</Title>,
  () => <MarkdownComponent contents={require('raw-loader!./events/1.md')} />,
  () => <ComponentDemo
    title="Esimerkki: onClick-tapahtumakäsittely"
    src={require('!raw!./events/Buttons.jsx')}
    element={require('./events/Buttons.jsx').default} />,
  () => <MarkdownComponent contents={require('raw-loader!./events/2.md')} />,

  // Props & propTypes

  () => <Title>Komponenttien tilanhallinta</Title>,
  () => <MarkdownComponent contents={require('raw-loader!./props-and-state/1.md')} />,
  () => <MarkdownComponent contents={require('raw-loader!./props-and-state/2.md')} />,
  () => <Soap title="Puuttuvat propTypes-määrittelyt" />,
  () => <ComponentDemo
    title="Esimerkki: Propertyjen välittäminen"
    src={require('!raw!./props-and-state/PropsPassing.jsx')}
    element={require('./props-and-state/PropsPassing.jsx').default} />,

  // State

  () => <MarkdownComponent contents={require('raw-loader!./props-and-state/5-state.md')} />,
  () =>
    <ComponentDemo
      title="Esimerkki: Pudotusvalikko"
      src={require('!raw!./props-and-state/DropDown.jsx')}
      element={require('./props-and-state/DropDown.jsx').default} />,

  // Lifecycle methods & mixins

  () => <Title>Elinkaarimetodit</Title>,
  () => <MarkdownComponent contents={require('raw-loader!./lifecycle/1.md')} />,
  () => <MarkdownComponent contents={require('raw-loader!./lifecycle/2.md')} />,
  () => <MarkdownComponent contents={require('raw-loader!./lifecycle/3.md')} />,
  () => <MarkdownComponent contents={require('raw-loader!./lifecycle/4.md')} />,
  () => <MarkdownComponent contents={require('raw-loader!./lifecycle/5.md')} />,
  () =>
    <ComponentDemo
      title="Esimerkki: Mixin"
      src={require('!raw!./lifecycle/Mixin.jsx')}
      element={require('./lifecycle/Mixin.jsx').default} />,
  () => <MarkdownComponent contents={require('raw-loader!./lifecycle/6.md')} />,
  () =>
    <ComponentDemo
      title="Esimerkki: Ulkoisten kirjastojen käyttäminen"
      src={require('!raw!./lifecycle/ExternalLib.jsx')}
      element={require('./lifecycle/ExternalLib.jsx').default} />,

  // Component design

  () => <Title>React-komponentin suunnittelu</Title>,
  () => <MarkdownComponent contents={require('raw-loader!./designing-component/1.md')} />,
  () => <MarkdownComponent contents={require('raw-loader!./designing-component/2.md')} />,
  () =>
    <ComponentDemo
      title="Esimerkki: Elokuvahaku / Input"
      src={require('!raw!./designing-component/Input.jsx')}
      element={require('./designing-component/Input.jsx').InputDemo} />,
  () =>
    <ComponentDemo
      title="Esimerkki: Elokuvahaku / Results"
      src={require('!raw!./designing-component/Results.jsx')}
      element={require('./designing-component/Results.jsx').ResultsDemo} />,
  () =>
    <ComponentDemo
      title="Esimerkki: Elokuvahaku / MovieSearch"
      src={require('!raw!./designing-component/MovieSearch.jsx')}
      element={require('./designing-component/MovieSearch.jsx').default} />,

  // State handling libraries

  () => <Title>Tilanhallintakirjastot</Title>,
  () => <MarkdownComponent contents={require('raw-loader!./state-containers/1.md')} />,
  () => <MarkdownComponent contents={require('raw-loader!./state-containers/2.md')} />,
  () => <MarkdownComponent contents={require('raw-loader!./state-containers/2-1.md')} />,
  () => <MarkdownComponent contents={require('raw-loader!./state-containers/3.md')} />,
  () =>
    <ComponentDemo
      title="Esimerkki: MovieSearch Redux"
      src={require('!raw!./state-containers/MovieSearchRedux.jsx')}
      element={require('./state-containers/MovieSearchRedux.jsx').default} />,

  // Testing React components

  () => <Title>Testaaminen</Title>,
  () => <MarkdownComponent contents={require('raw-loader!./testing/1.md')} />,
  () => <MarkdownComponent contents={require('raw-loader!./testing/2.md')} />,
  () => <MarkdownComponent contents={require('raw-loader!./testing/3.md')} />,
  () => <MarkdownComponent contents={require('raw-loader!./testing/4.md')} />,
  () => <Title>Yksikkötestidemo</Title>,
  () => <Title>React storybook</Title>,
  () => <MarkdownComponent contents={require('raw-loader!./testing/5.md')} />,
  () => <MarkdownComponent contents={require('raw-loader!./testing/6.md')} />,
  () => <Title>React storybook demo</Title>,

  // Installation

  () => <Title>Reactin asentaminen</Title>,
  () => <MarkdownComponent contents={require('raw-loader!./installation/1.md')} />,
  () => <MarkdownComponent contents={require('raw-loader!./installation/2.md')} />,

  // Server side rendering

  () => <Title>Serveripuolen renderöinti</Title>,
  () => <MarkdownComponent contents={require('raw-loader!./server-side/1.md')} />,

  // Best practises & more resources

  () => <Title>"Best practises"</Title>,
  () => <MarkdownComponent contents={require('raw-loader!./the-end/1.md')} />,
  () => <MarkdownComponent contents={require('raw-loader!./the-end/2.md')} />,

  // The end

  () => <Title>Kysymyksiä?</Title>,
  () => <End />
];
