import './cover.scss';
const CoverImage = require('./React.js_logo.svg');
import * as React from 'react';

export default class Cover extends React.Component {
  render() {
    return (
      <div className="cover">
        <div className="cover-inner">
          <div>
            <img className="cover-image" src={CoverImage} />
            <h1 className="cover-title">Reactin perusteet</h1>
          </div>
        </div>
      </div>
    );
  }
}