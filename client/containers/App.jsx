import React from 'react';
import NavigationBar from '../components/NavigationBar';

export default class App extends React.Component {
  getCurrentPath() {
    return this.props.location.pathname;
  }

  render() {
    console.log("App: this.props", this.props);
    console.log("App: this.props.params", this.props.params);
    const { route } = this.props;
    let totalPages = route.totalPages();
    let currentPage = route.currentPageNumber(this.getCurrentPath()) + 1;
    
    return (
      <div id="app" className="mui-container-fluid">
        <NavigationBar
          currentPageNumber={currentPage}
          totalPages={totalPages}
          onPrevious={() => route.previousPage(this.getCurrentPath())}
          onNext={() => route.nextPage(this.getCurrentPath())} />
        {this.props.children}
      </div>
    );
  }
}
