import React from 'react';
import { SLIDES } from './slides';

export default class Slide extends React.Component {
  render() {
    const slideNumber = this.props.params.id || 0;
    return SLIDES[slideNumber]();
  }
}
