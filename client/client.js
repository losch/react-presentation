import React from 'react';
import ReactDOM from 'react-dom';
import { Router, browserHistory } from 'react-router';
import routes from './routes';
import './vendor/mui-0.6.0/css/mui.css';
import './styles.scss';

ReactDOM.render(<Router history={browserHistory}
                        children={routes()} />,
                document.getElementById('app'));
