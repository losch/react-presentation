/* eslint-disable react/no-danger */

import * as React from 'react';
import marked from 'marked';
import './MarkdownComponent.scss';
import hljs from '../vendor/highlight/highlight.min';
import '../vendor/highlight/default.min.css';

marked.setOptions({
  highlight: (code) => hljs.highlightAuto(code).value
});

export default class MarkDownComponent extends React.Component {
  render() {
    const markdown = {__html: marked(this.props.contents, {sanitize: true})};
    return <div className="markdown-component"
                dangerouslySetInnerHTML={markdown} />;
  }
}

MarkDownComponent.propTypes = {
  contents: React.PropTypes.string
};
