import React from 'react';
import './NavigationBar.scss';

const LEFT_ARROW_KEY = '37';
const RIGHT_ARROW_KEY = '39';

export default class NavigationBar extends React.Component {
  constructor(props) {
    super(props);
  }

  checkKey(e) {
    e = e || window.event;

    if (e.keyCode == LEFT_ARROW_KEY) {
      this.props.onPrevious();
    }
    else if (e.keyCode == RIGHT_ARROW_KEY) {
      this.props.onNext();
    }
  }

  componentDidMount() {
    document.onkeydown = (e) => this.checkKey(e);
  }

  componentWillUnmount() {
    document.onkeydown = null;
  }

  render() {
    const { totalPages, currentPageNumber } = this.props;
    let progress = Math.ceil(currentPageNumber / totalPages * 100);

    console.log('**** progress', totalPages, currentPageNumber, progress);

    return (
      <div className="progress-bar-container">
        <div className="progress-bar" style={{width: '' + progress + '%'}} />
      </div>
    );
  }
}
