import React from 'react';

export default class Counter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 0,
      isDynamic: false
    };
  }

  componentDidMount() {
    setTimeout(() => this.increase(), 1000);
  }

  increase() {
    this.setState({
      count: this.state.count + 1,
      isDynamic: true
    });

    setTimeout(() => this.increase(), 1000);
  }

  render() {
    let style = {
      backgroundColor: this.state.isDynamic ? 'white' : 'yellow'
    };

    return <div style={style}>{this.props.title}: {this.state.count}</div>
  }
}
