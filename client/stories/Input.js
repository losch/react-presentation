import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import Input from '../containers/slides/designing-component/Input.jsx';
import '../containers/slides/designing-component/MovieSearch.scss';

storiesOf('Input', module)
  .add('is empty', () => (
    <Input value='' onChange={action('query changed')} />
  ))
  .add('has content', () => (
    <Input value='Eggs, ham, bacon' onChange={action('query changed')} />
  ));
