import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import { DropDownButton } from '../containers/slides/props-and-state/DropDown.jsx';

storiesOf('DropDownButton', module)
  .add('has title and contents', () => (
    <DropDownButton text="Dropdown menu">
      <p>Eggs</p>
      <p>Ham</p>
      <p>Bacon</p>
    </DropDownButton>
  ))
  .add('has no contents', () => (
    <DropDownButton text="Dropdown menu" />
  ));
