import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import Results from '../containers/slides/designing-component/Results.jsx';
import '../containers/slides/designing-component/MovieSearch.scss';

let results = {
  "Search": [
    {
      "Title": "Socks: Episode V - The Tumble Dryer Strikes Back",
      "Year": "1980",
      "imdbID": "tt1392190",
      "Poster": "/sock.jpg"
    },
    {
      "Title": "Matka maan keskipisteeseen",
      "Year": "2015",
      "imdbID": "tt1392190",
      "Poster": "/warning.jpg"
    },
    /*
    {
      "Title": "Gravity",
      "Year": "2013",
      "imdbID": "tt1392190",
      "Poster": "/warning.jpg"
    }
    */
  ]
};

storiesOf('Results', module)
  .add('has no results', () => (
    <Results isSearching={false} results={{}} />
  ))
  .add('is searching', () => (
    <Results isSearching={true} results={{}} />
  ))
  .add('is showing results', () => (
    <Results isSearching={false} results={results} />
  ));
