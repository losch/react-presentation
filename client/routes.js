/* eslint-disable no-console */

import findIndex from 'lodash/findIndex';
import React from 'react';
import { Route, IndexRoute, browserHistory } from 'react-router';
import App from './containers/App';
import Slide from './containers/Slide';
import Cover from './containers/Cover';
import { SLIDES } from './containers/slides';

const SLIDE_ROUTES = SLIDES.map((slide, i) => '/slides/' + i);
const PAGE_ROUTES = ['/'].concat(SLIDE_ROUTES);

function nextPage(path) {
  let currentPage = findIndex(PAGE_ROUTES, route => route === path);
  let nextPage = currentPage + 1;
  if (nextPage < PAGE_ROUTES.length) {
    browserHistory.push(PAGE_ROUTES[nextPage]);
  }
}

function previousPage(path) {
  let currentPage = findIndex(PAGE_ROUTES, route => route === path);
  if (currentPage > 0) {
    browserHistory.push(PAGE_ROUTES[currentPage - 1]);
  }
}

function currentPageNumber(path) {
  return findIndex(PAGE_ROUTES, route => route === path);
}

function totalPages() {
  return PAGE_ROUTES.length;
}

export default function routes() {
  return (
    <Route path="/"
           component={App}
           currentPageNumber={currentPageNumber}
           totalPages={totalPages}
           nextPage={nextPage}
           previousPage={previousPage}>
      <IndexRoute component={Cover}
                  currentPageNumber={currentPageNumber}
                  totalPages={totalPages}
                  nextPage={nextPage}
                  previousPage={previousPage} />
      <Route path="/slides/:id"
             component={Slide}
             currentPageNumber={currentPageNumber}
             totalPages={totalPages}
             nextPage={nextPage}
             previousPage={previousPage} />
    </Route>
  );
}