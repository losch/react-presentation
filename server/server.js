require('babel-register');
var Hapi = require('hapi');

var server = new Hapi.Server();
server.connection({
  host: 'localhost',
  port: 8000
});

server.register([
  { register: require('inert') },
  { register: require('h2o2') },
  { register: require('vision') }], (err) => {

  if (err) return console.error(err);

  // React-rendering view engine
  server.views({
    engines: {
      jsx: require('hapi-react-views')
    },
    relativeTo: __dirname,
    path: '../client/containers'
  });

  // Movie search proxy
  server.route([
    {
      method: 'GET',
      path: '/moviesearch/',
      handler: {
        proxy: {
          mapUri: function(request, callback) {
            callback(null, 'http://www.omdbapi.com/' + request.url.search);
          }
        }
      }
    }
  ]);

  // Static files
  server.route({
    method: 'GET',
    path: '/static/{param*}',
    handler: {
      directory: {
        path: 'static',
        index: ['index.html']
      }
    }
  });

  // Main app route
  server.route({
    method: 'GET',
    path: '/{param*}',
    handler: {
      view: 'Index'
    }
  });

  server.start(() => {
    console.log('Server running at: ' + server.info.uri);
  });
});
