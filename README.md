React presentation (in Finnish)
===============================

Requirements
------------

* [NodeJS and npm package manager](https://nodejs.org/en/download/)

Usage
-----

1. Install dependencies with ```npm install```
2. Start up backend server ```npm start```
3. Start up frontend development server ```npm run dev-server```
4. Open browser to ```http://localhost:9000```
5. Use left and right arrows to navigate between slides

Unit tests
----------

Run `npm run test` for running unit tests.
Unit tests are located under `client/test` directory.

React storybook
---------------

React storybook can be run with `npm run storybook`
Then open browser to http://localhost:9001

Stories are located under `client/stories`
