import { configure } from '@kadira/storybook';
import '../client/vendor/mui-0.6.0/css/mui.css';
import '../client/styles.scss';

function loadStories() {
  require('../client/stories/button');
  require('../client/stories/DropDownButton');
  require('../client/stories/Input');
  require('../client/stories/Results');
}

configure(loadStories, module);
